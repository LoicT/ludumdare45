﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobInfo : MonoBehaviour
{
    private static readonly Color NOT_ENOUGH_COLOR = new Color(224 / 255f, 60 / 255f, 35 / 255f);

    public Text item;
    public Text salary;

    public Text APToJob;
    public Button APPlus;
    public Button APMinus;

    public Button buttonQuit;

    public void Initialize(Profession profession)
    {
        this.gameObject.name = profession.name;
        this.item.text = profession.name;
        this.salary.text = profession.salary.ToString();
        this.APToJob.text = Manager.Game.Protagonist.actionPointsInProfession.ToString();

        this.buttonQuit.onClick.AddListener(delegate
        {
            Manager.Professions.QuitProfession();

            Manager.UI.UpdateApp();
            Manager.UI.ShowProtagonist();
        });

        this.APPlus.onClick.AddListener(delegate
        {
            Manager.Game.Protagonist.AddPointToProfession();
            Manager.UI.UpdateApp();
            Manager.UI.ShowProtagonist();
        });

        this.APMinus.onClick.AddListener(delegate
        {
            Manager.Game.Protagonist.RemovePointToProfession();
            Manager.UI.UpdateApp();
            Manager.UI.ShowProtagonist();
        });

        this.gameObject.SetActive(true);
    }
}
