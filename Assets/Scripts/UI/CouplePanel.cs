﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CouplePanel : MonoBehaviour
{
    public delegate void ChoiceDelegate();

    [FoldoutGroup("Protagonist")]
    public Image prBody;

    [FoldoutGroup("Protagonist")]
    public Image prMouth;

    [FoldoutGroup("Protagonist")]
    public Image prEyes;

    [FoldoutGroup("Protagonist")]
    public Image prBrows;

    [FoldoutGroup("Protagonist")]
    public Image prHairBack;

    [FoldoutGroup("Protagonist")]
    public Image prHair;

    [FoldoutGroup("Protagonist")]
    public Image prClothes;

    [FoldoutGroup("Protagonist")]
    public Text prName;

    [FoldoutGroup("Protagonist")]
    public RectTransform prGaugesRoot;

    [FoldoutGroup("Target")]
    public GameObject targetPanel;

    [FoldoutGroup("Target")]
    public Image trBody;

    [FoldoutGroup("Target")]
    public Image trMouth;

    [FoldoutGroup("Target")]
    public Image trEyes;

    [FoldoutGroup("Target")]
    public Image trBrows;

    [FoldoutGroup("Target")]
    public Image trHairBack;

    [FoldoutGroup("Target")]
    public Image trHair;

    [FoldoutGroup("Target")]
    public Image trClothes;

    [FoldoutGroup("Target")]
    public Text trName;

    [FoldoutGroup("Target")]
    public RectTransform trGaugesRoot;

    private App currentApp;
    private Dictionary<Characteristics.Name, Slider> gauges;

    public void Initialize(Protagonist protagonist, Target target)
    {
        this.ShowProtagonist(protagonist);
        if (target != null)
            this.ShowTarget(target);
        this.gameObject.SetActive(true);
    }

    public void ShowProtagonist(Protagonist protagonist)
    {
        this.ShowBodyPart(this.prBody, protagonist.totalBody.Body, protagonist);
        this.ShowBodyPart(this.prMouth, protagonist.totalBody.Mouth, protagonist);
        this.ShowBodyPart(this.prEyes, protagonist.totalBody.Eyes, protagonist);
        this.ShowBodyPart(this.prBrows, protagonist.totalBody.Brows, protagonist);
        this.ShowBodyPart(this.prHairBack, protagonist.totalBody.HairBack, protagonist);
        this.ShowBodyPart(this.prHair, protagonist.totalBody.Hair, protagonist);
        this.ShowBodyPart(this.prClothes, protagonist.totalBody.Clothes, protagonist);
        this.prName.text = protagonist.name;
        Manager.UI.InitializeGauges(protagonist, this.prGaugesRoot);
    }

    public void ShowTarget(Target target)
    {
        this.ShowBodyPart(this.trBody, target.totalBody.Body, target);
        this.ShowBodyPart(this.trMouth, target.totalBody.Mouth, target);
        this.ShowBodyPart(this.trEyes, target.totalBody.Eyes, target);
        this.ShowBodyPart(this.trBrows, target.totalBody.Brows, target);
        this.ShowBodyPart(this.trHairBack, target.totalBody.HairBack, target);
        this.ShowBodyPart(this.trHair, target.totalBody.Hair, target);
        this.ShowBodyPart(this.prClothes, target.totalBody.Clothes, target);
        this.trName.text = target.name;
        Manager.UI.InitializeGauges(target, this.trGaugesRoot);
        this.targetPanel.SetActive(true);
    }

    public void ShowBodyPart(Image image, BodyPart part, Character character)
    {
        image.sprite = part.appearance.Sprite(character);
        image.color = part.Color();
    }
}
