﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    private static readonly Color NOT_ENOUGH_COLOR = new Color(224 / 255f, 60 / 255f, 35 / 255f);

    public Text item;
    public Text moneyCost;

    public Image moneyCostBg;
    public GameObject[] shadows;

    public Button button;

    public void Initialize(Item item)
    {
        this.gameObject.name = item.name;
        this.item.text = item.name;
        this.moneyCost.text = item.cost.ToString();

        bool canBuy = item.CanBuy();
        this.Toggle(canBuy);

        if (!canBuy)
            this.moneyCostBg.color = NOT_ENOUGH_COLOR;

        this.button.onClick.AddListener(delegate
        {
            Manager.Game.Protagonist.Buy(item);
            Manager.UI.UpdateApp();
        });

        this.gameObject.SetActive(true);
    }

    private void Toggle(bool value)
    {
        this.button.interactable = value;

        foreach (GameObject shadow in this.shadows)
            shadow.SetActive(value);
    }
}
