﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionButton : MonoBehaviour
{
    private static readonly Color NOT_ENOUGH_COLOR = new Color(224 / 255f, 60 / 255f, 35 / 255f);

    public Text action;
    public Text apCost;
    public Text moneyCost;
    public GameObject moneyCostObject;
    public GameObject car;

    public Image apCostBg;
    public Image moneyCostBg;
    public Image carBg;
    public GameObject[] shadows;

    public Button button;

    public void Initialize(Action action, object parameter = null)
    {
        Target target = null;
        if (parameter != null)
        {
            target = (Target)parameter;
        }

        this.gameObject.name = action.name;
        this.action.text = target != null ? (target.name + ", " + target.age) :  action.name;
        this.apCost.text = action.apCost.ToString();

        if (action.moneyCost == 0)
        {
            this.moneyCostObject.SetActive(false);
        }
        else
        {
            this.moneyCost.text = action.moneyCost.ToString();
            if (action.moneyCost > Manager.Game.Protagonist.possessions.Money)
                this.moneyCostBg.color = NOT_ENOUGH_COLOR;
        }
        
        this.Toggle(action.IsPossible());

        if (action.apCost > Manager.Game.Protagonist.ActionPoints)
            this.apCostBg.color = NOT_ENOUGH_COLOR;

        this.button.onClick.AddListener(delegate { Manager.Game.Protagonist.PerformAction(action, parameter); Manager.UI.UpdateApp(); });

        bool needsVehicle = action is GoToAction && (action as GoToAction).needsVehicle;
        this.car.SetActive(needsVehicle);
        if (!Manager.Game.Protagonist.possessions.HasVehicle())
        {
            this.carBg.color = NOT_ENOUGH_COLOR;
        }
        
        this.gameObject.SetActive(true);
    }

    private void Toggle(bool value)
    {
        this.button.interactable = value;

        foreach (GameObject shadow in this.shadows)
            shadow.SetActive(value);
    }
}
