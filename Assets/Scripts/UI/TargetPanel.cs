﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetPanel : MonoBehaviour
{
    public Image trBody;
    public Image trMouth;
    public Image trEyes;
    public Image trBrows;
    public Image trHairBack;
    public Image trHair;
    public Image trClothes;
    public Text trNameAge;
    public Image heart;
    public Slider prestige;
    public RectTransform gaugesRoot;

    public void Initialize(Target target)
    {
        this.gameObject.name = target.name;
        this.trNameAge.text = target.name + ", " + target.Age;

        float affinity = target.Affinity / Target.MAX_AFFINITY;
        this.heart.rectTransform.localScale = Vector3.one * Mathf.Clamp(affinity, 0.2f, 1);
        this.heart.color = Manager.UI.affinityColors.Evaluate(affinity);

        if (this.prestige != null)
        {
            this.prestige.value = target.characteristics.Prestige / Characteristics.MAX_PRESTIGE;
        }

        Manager.UI.ShowBodyPart(this.trBody, target.totalBody.Body, target);
        Manager.UI.ShowBodyPart(this.trMouth, target.totalBody.Mouth, target);
        Manager.UI.ShowBodyPart(this.trEyes, target.totalBody.Eyes, target);
        Manager.UI.ShowBodyPart(this.trBrows, target.totalBody.Brows, target);
        Manager.UI.ShowBodyPart(this.trHairBack, target.totalBody.HairBack, target);
        Manager.UI.ShowBodyPart(this.trHair, target.totalBody.Hair, target);
        Manager.UI.ShowBodyPart(this.trClothes, target.totalBody.Clothes, target);

        if (this.gaugesRoot != null)
        {
            Manager.UI.InitializeGauges(target, this.gaugesRoot);
        }

        this.gameObject.SetActive(true);
    }
}
