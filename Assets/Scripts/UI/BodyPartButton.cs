﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BodyPartButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private static readonly Color NOT_ENOUGH_COLOR = new Color(224 / 255f, 60 / 255f, 35 / 255f);

    public Text appearance;
    public Text moneyCost;
    public Image moneyCostBg;
    
    public GameObject[] shadows;

    public Button button;

    private AppearanceItem appearanceItem;

    public void Initialize(AppearanceItem item)
    {
        this.appearanceItem = item;
        int cost = item.appearance.beauty * 10;
        this.gameObject.name = item.name;
        this.appearance.text = item.name;
        this.moneyCost.text = cost.ToString();

        bool canBuy = Manager.Game.Protagonist.possessions.Money >= cost;
        this.Toggle(canBuy);

        if (!canBuy)
            this.moneyCostBg.color = NOT_ENOUGH_COLOR;

        this.button.onClick.AddListener(delegate
        {
            Manager.Game.Protagonist.totalBody.SetAppearance(item.appearance, item.part);
            Manager.Game.Protagonist.possessions.Money -= cost;
            Manager.UI.ShowProtagonist();
            Manager.UI.UpdateApp();
        });

        this.gameObject.SetActive(true);
    }

    private void Toggle(bool value)
    {
        this.button.interactable = value;

        foreach (GameObject shadow in this.shadows)
            shadow.SetActive(value);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        switch (this.appearanceItem.part)
        {
            case TotalBody.Name.Hair:
                Manager.UI.prHair.sprite = this.appearanceItem.appearance.Sprite(Manager.Game.Protagonist);
                break;
            case TotalBody.Name.Brows:
                Manager.UI.prBrows.sprite = this.appearanceItem.appearance.Sprite(Manager.Game.Protagonist);
                break;
            case TotalBody.Name.Clothes:
                Manager.UI.prClothes.sprite = this.appearanceItem.appearance.Sprite(Manager.Game.Protagonist);
                break;
            default:
                break;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Manager.UI.ShowProtagonist();
    }
}
