﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JobButton : MonoBehaviour
{
    public Text item;
    public Text salary;
    public GameObject[] shadows;

    public Button button;

    public void Initialize(Profession profession)
    {
        this.gameObject.name = profession.name;
        this.item.text = profession.name;
        this.salary.text = profession.salary.ToString();

        this.button.onClick.AddListener(delegate
        {
            Manager.Game.Protagonist.Job = profession;
            Manager.UI.UpdateApp();
        });

        if (Manager.Game.Protagonist.Job != null)
        {
            this.Toggle(false);
        }

        this.gameObject.SetActive(true);
    }

    private void Toggle(bool value)
    {
        this.button.interactable = false;

        foreach (GameObject shadow in this.shadows)
            shadow.SetActive(value);
    }
}
