﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RandomCondition", menuName = "Condition/Random Condition")]
public class RandomCondition : ValueCondition
{
    public int exclusiveBound;

    public override bool IsConditionFulfilled()
    {
        return this.IsTrue(Random.Range(0, this.exclusiveBound));
    }
}
