using UnityEngine;

[CreateAssetMenu(fileName = "DeficitCondition", menuName = "Condition/Deficit Condition")]
public class DeficitCondition : ValueCondition
{

    public override bool IsConditionFulfilled()
    {
        return this.IsTrue(Manager.Dialogue.interlocutor.PreferencesDeficit());
    }
}