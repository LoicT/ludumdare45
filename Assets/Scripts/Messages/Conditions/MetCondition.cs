using UnityEngine;

[CreateAssetMenu(fileName = "MetCondition", menuName = "Condition/Met Condition")]
public class MetCondition : Condition
{
    public override bool IsConditionFulfilled()
    {
        return Manager.Game.Protagonist.Acquaintances.Contains(Manager.Dialogue.interlocutor);
    }
}