﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ValueCondition : Condition
{
    public Operation operation;
    public int target = 1;

    public enum Operation
    {
        Equals,
        SuperiorTo,
        InferiorTo,
        SuperiorOrEqualTo,
        InferiorOrEqualTo,
        DifferentFrom
    }

    protected bool IsTrue(int value)
    {
        switch (this.operation)
        {
            case Operation.Equals:
                return value == this.target;
            case Operation.DifferentFrom:
                return value != this.target;
            case Operation.SuperiorTo:
                return value > this.target;
            case Operation.InferiorTo:
                return value < this.target;
            case Operation.SuperiorOrEqualTo:
                return value >= this.target;
            case Operation.InferiorOrEqualTo:
                return value <= this.target;
            default:
                return false;
        }
    }
}
