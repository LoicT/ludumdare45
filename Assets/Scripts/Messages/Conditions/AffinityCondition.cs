using UnityEngine;

[CreateAssetMenu(fileName = "AffinityCondition", menuName = "Condition/Affinity Condition")]
public class AffinityCondition : ValueCondition
{

    public override bool IsConditionFulfilled()
    {
        return this.IsTrue((int)Manager.Dialogue.interlocutor.Affinity);
    }
}