﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ChoiceCondition", menuName = "Condition/Choice Condition")]
public class ChoiceCondition : Condition
{
    public int choice;

    public override bool IsConditionFulfilled()
    {
        return Manager.Dialogue.LastChoice == choice;
    }
}
