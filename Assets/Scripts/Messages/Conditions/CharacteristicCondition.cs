using UnityEngine;

[CreateAssetMenu(fileName = "CharacteristicCondition", menuName = "Condition/Characteristic Condition")]
public class CharacteristicCondition : ValueCondition
{
    public Characteristics.Name characteristicsName;

    public override bool IsConditionFulfilled()
    {
        return this.IsTrue(Manager.Game.Protagonist.characteristics.GetCharacteristicValue(characteristicsName));
    }
}