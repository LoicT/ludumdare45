using UnityEngine;

[CreateAssetMenu(fileName = "PreferenceCondition", menuName = "Condition/Preference Condition")]
public class PreferenceCondition : ValueCondition
{

    public Characteristics.Name characteristiqueName;


    public override bool IsConditionFulfilled()
    {
        CharacteristicPreference charaPreference = Manager.Dialogue.interlocutor.preferences.Find((Preference preference) =>
        {
            CharacteristicPreference chara = preference as CharacteristicPreference;
            if (chara == null)
            {
                return false;
            }
            else
            {
                if (chara.characteristic == characteristiqueName)
                    return true;
            }
            return false;
        }) as CharacteristicPreference;

        return (charaPreference != null ? this.IsTrue(charaPreference.Quantity) : false);
    }
}