using UnityEngine;

[CreateAssetMenu(fileName = "MoneyCondition", menuName = "Condition/Money Condition")]
public class MoneyCondition : ValueCondition
{
    public override bool IsConditionFulfilled()
    {
        return this.IsTrue(Manager.Game.Protagonist.possessions.Money);
    }
}