﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemCondition", menuName = "Condition/Item Condition")]
public class ItemCondition : Condition
{
    public Item item;
    public bool has = true;

    public override bool IsConditionFulfilled()
    {
        return Manager.Game.Protagonist.possessions.HasItem(item) == this.has;
    }
}
