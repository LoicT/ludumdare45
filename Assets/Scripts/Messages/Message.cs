﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Message", menuName = "Message")]
public class Message : ScriptableObject
{
    [System.Serializable]
    public class NextMessage
    {
        public List<Condition> conditions;
        public Message message;

        public bool AreAllConditionsFulfilled()
        {
            foreach (Condition condition in this.conditions)
            {
                if (!condition.IsConditionFulfilled())
                {
                    return false;
                }
            }
            return true;
        }
    }

    [System.Serializable]
    public class Choice
    {
        public List<Condition> conditions;
        public string choice;

        public bool AreAllConditionsFulfilled()
        {
            foreach (Condition condition in this.conditions)
            {
                if (!condition.IsConditionFulfilled())
                {
                    return false;
                }
            }
            return true;
        }
    }

    [MultiLineProperty]
    public string message;

    public List<NextMessage> nextMessages;

    public List<Choice> choices;

    public List<Item> itemsLost;
    public List<Item> itemsObtained;
    public int money;
    public int affinity;

    public string Text
    {
        get
        {
            return this.message
                .Replace("{protagonist}", Manager.Game.Protagonist.name)
                .Replace("{target}", Manager.Dialogue.interlocutor.name);
        }
    }

    public List<string> Choices
    {
        get
        {
            return new List<string>(this.choices.FindAll(x => x.AreAllConditionsFulfilled()).Select(x => x.choice));
        }
    }

    public Message GetNextMessage()
    {
        foreach (NextMessage nextMessage in this.nextMessages)
        {
            if (nextMessage.AreAllConditionsFulfilled())
            {
                return nextMessage.message;
            }
        }
        return null;
    }

    public void Consequences()
    {
        Manager.Game.Protagonist.possessions.items.UnionWith(itemsObtained);
        Manager.Game.Protagonist.possessions.items.ExceptWith(itemsLost);
        Manager.Game.Protagonist.possessions.Money += this.money;
        if (this.affinity != 0)
            Manager.Dialogue.interlocutor.AddAffinity(this.affinity);
        Manager.UI.ShowProtagonist();
        Manager.Dialogue.ShowInterlocutor();
    }
}
