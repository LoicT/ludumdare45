﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LinkedBodyPart : BodyPart
{
    public MainBodyPart mainPart;

    public LinkedBodyPart(MainBodyPart mainPart, Appearance appearance) : base(appearance)
    {
        this.mainPart = mainPart;
    }

    public override Color Color()
    {
        return this.mainPart.Color();
    }
}
