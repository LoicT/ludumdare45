﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Appearance", menuName = "Appearance", order = 0)]
public class SingleAppearance : Appearance
{
    public Sprite sprite;

    public override Sprite Sprite(Character character)
    {
        return this.sprite;
    }
}
