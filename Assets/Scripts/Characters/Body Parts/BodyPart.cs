﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BodyPart
{

    public Appearance appearance;

    public abstract Color Color();

    public BodyPart(Appearance appearance)
    {
        this.appearance = appearance;
    }

    protected CharactersVisualsManager.BodyPartVisuals PossibleVisuals(bool target = false)
    {
        string partName = this.GetType().ToString();
        List<CharactersVisualsManager.BodyPartVisuals> visuals = target ? Manager.CharactersVisuals.targetsVisuals : Manager.CharactersVisuals.protagonistsVisuals;

        return visuals.Find(x => x.part.ToString() == partName);
    }
}
