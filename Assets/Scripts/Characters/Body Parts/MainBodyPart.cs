﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MainBodyPart : BodyPart
{
    public Color color;

    public MainBodyPart(Appearance appearance) : base(appearance)
    {
        this.color = this.PossibleVisuals().RandomColor();
    }

    public override Color Color()
    {
        return this.color;
    }
}
