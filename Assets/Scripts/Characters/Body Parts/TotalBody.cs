using UnityEngine;

public class TotalBody
{
    public static int TOTAL_BODY_PART
    {
        get
        {
            return System.Enum.GetValues(typeof(Name)).Length;
        }
    }

    public enum Name
    {
        Body, Hair, Eyes, Brows, Mouth, HairBack, Clothes
    }

    public BodyPart GetRandomBodyPartFromTargetedBeauty(Name name, int beauty, bool target)
    {
        Appearance appearance = Manager.CharactersVisuals.GetBodyPartVisualFromName(name, target).RandomAppearance(beauty);
        switch (name)
        {
            case Name.Body:
                return new Body(appearance);
            case Name.Hair:
                return new Hair(appearance);
            case Name.Eyes:
                return new Eyes(appearance);
            case Name.Brows:
                return new Brows(this.Hair, appearance);
            case Name.Mouth:
                return new Mouth(this.Body, appearance);
            case Name.HairBack:
                return new HairBack(this.Hair, appearance);
            case Name.Clothes:
                return new Clothes(appearance);
            default:
                return null;
        }
    }

    public static TotalBody GenerateBodyWithBeauty(Character character)
    {
        TotalBody result = new TotalBody(character);

        float[] values = new float[TOTAL_BODY_PART];
        float total = 0;
        for (int i = 0; i < values.Length; i++)
        {
            values[i] = Random.Range(0f, 1f);
            total += values[i];
        }

        float targetBeauty = character.characteristics.Beauty;
        character.characteristics.Beauty = 0;

        int j = 0;
        foreach (Name name in System.Enum.GetValues(typeof(Name)))
        {
            result.SetBodyPart(
                result.GetRandomBodyPartFromTargetedBeauty(name, Mathf.CeilToInt((values[j] / total) * targetBeauty),
                character as Target != null),
                (int)name);
            j++;
        }

        return result;
    }

    private Character character;

    private BodyPart[] totalBody = new BodyPart[TOTAL_BODY_PART];

    private TotalBody(Character character)
    {
        this.character = character;
    }

    /**
    * Set the body part without changing the beauty
    * Used in the first set of the body parts
    */
    private void SetFirstBodyPart(Name name, BodyPart part)
    {
        this.totalBody[(int)name] = part;
    }

    private void SetBodyPart(BodyPart part, int index)
    {
        this.character.characteristics.Beauty += part.appearance.beauty - (this.totalBody[index] != null ? this.totalBody[index].appearance.beauty : 0);
        this.totalBody[index] = part;
    }

    public void SetAppearance(Appearance appearance, Name name)
    {
        this.character.characteristics.Beauty += appearance.beauty - (this.totalBody[(int)name] != null ? this.totalBody[(int)name].appearance.beauty : 0);
        this.totalBody[(int)name].appearance = appearance;
    }

    public BodyPart GetBodyPart(Name name)
    {
        return this.totalBody[(int)name];
    }

    public Body Body
    {
        get
        {
            return (Body)this.totalBody[(int)Name.Body];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Body);
        }
    }
    public Hair Hair
    {
        get
        {
            return (Hair)this.totalBody[(int)Name.Hair];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Hair);
        }
    }
    public Eyes Eyes
    {
        get
        {
            return (Eyes)this.totalBody[(int)Name.Eyes];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Eyes);
        }
    }
    public Brows Brows
    {
        get
        {
            return (Brows)this.totalBody[(int)Name.Brows];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Brows);
        }
    }
    public Mouth Mouth
    {
        get
        {
            return (Mouth)this.totalBody[(int)Name.Mouth];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Mouth);
        }
    }
    public HairBack HairBack
    {
        get
        {
            return (HairBack)this.totalBody[(int)Name.HairBack];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.HairBack);
        }
    }
    public Clothes Clothes
    {
        get
        {
            return (Clothes)this.totalBody[(int)Name.Clothes];
        }
        set
        {
            this.SetBodyPart(value, (int)Name.Clothes);
        }
    }

    public bool HasAppearance(Appearance appearance, Name name)
    {
        return this.totalBody[(int)name].appearance == appearance;
    }

}