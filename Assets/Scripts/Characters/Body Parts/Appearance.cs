using UnityEngine;

public abstract class Appearance : ScriptableObject
{
    public int beauty;

    public abstract Sprite Sprite(Character character);
}