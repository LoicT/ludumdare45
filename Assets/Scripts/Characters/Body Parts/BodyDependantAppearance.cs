﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BodyDependantAppearance", menuName = "BodyDependantAppearance", order = 0)]
public class BodyDependantAppearance : Appearance
{
    [System.Serializable]
    public class BodyAppearance
    {
        public Appearance body;
        public Sprite clothes;
    }

    public List<BodyAppearance> appearances;

    public override Sprite Sprite(Character character)
    {
        return this.appearances.Find(x => x.body == character.totalBody.Body.appearance).clothes;
    }
}
