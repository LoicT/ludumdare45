using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(fileName = "Job", menuName = "Job", order = 0)]
public class Profession : ScriptableObject
{
    public int salary = 10;
    public int minimumActionPoints = 1;

    public List<Condition> conditions;

    public bool CanTake()
    {
        foreach (Condition condition in this.conditions)
        {
            if (!condition.IsConditionFulfilled())
            {
                return false;
            }
        }
        return true;
    }
}