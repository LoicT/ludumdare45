﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Target : Character
{
    public static readonly float STARTING_AFFINITY = 20f;
    public static readonly float MAX_AFFINITY = 100f;
    public static readonly float AFFINITY_REDUCTION_PER_MONTH = 0.75f;
    public static readonly float AFFINITY_MULTIPLIER_AFTER_PREGNANCY = 0.7f;
    public static readonly int TIME_BETWEEN_CALLS = 3;
    public static readonly int TIME_BEFORE_FORGETTING = 9;

    private float affinity = Target.STARTING_AFFINITY;
    public float Affinity
    {
        get
        {
            return this.affinity;
        }

        private set
        {
            this.affinity = Mathf.Clamp(value, 0, Target.MAX_AFFINITY);
        }
    }

    public bool Pregnant { get; private set; }
    private int pregnancyStart;

    private int lastCall = -1;
    public int LastInteraction { get; set; }

    public List<Preference> preferences = new List<Preference>();

    public Target() : base(Manager.Name.GetName(), Manager.Name.GetAge())
    {
        
    }

    public bool ArePreferencesMet(Protagonist protagonist)
    {
        foreach (Preference preference in this.preferences)
        {
            if (!preference.ArePreferencesMet())
            {
                return false;
            }
        }
        return true;
    }

    public int PreferencesDeficit()
    {
        return this.preferences.Aggregate(0, (acc, preference) => acc += preference.Deficit());
    }

    public override void NextTurn()
    {
        this.affinity -= Target.AFFINITY_REDUCTION_PER_MONTH;
    }

    public void GetPregnant()
    {
        this.Pregnant = true;
        this.pregnancyStart = Manager.Game.CurrentTurn;
    }

    public bool CanGiveBirth()
    {
        return this.Pregnant && Manager.Game.CurrentTurn - this.pregnancyStart >= 8;
    }

    public void StopPregnancy()
    {
        this.Pregnant = false;
        this.affinity *= AFFINITY_MULTIPLIER_AFTER_PREGNANCY;
    }

    public void Call()
    {
        this.lastCall = Manager.Game.CurrentTurn;
    }

    public bool CanCall()
    {
        return this.lastCall == -1 || Manager.Game.CurrentTurn - this.lastCall >= TIME_BETWEEN_CALLS;
    }

    public void AddAffinity(float value)
    {
        float prestige = Mathf.Max(this.characteristics.Prestige - Manager.Game.Protagonist.characteristics.Prestige, 0);
        prestige = value > 0 ? prestige : Characteristics.MAX_PRESTIGE - prestige;
        float multiplier = 1.8f * Mathf.Log10(1f / (prestige + 1)) + 1.8f;
        this.Affinity += (value * multiplier);
    }
}
