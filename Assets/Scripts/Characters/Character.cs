using UnityEngine;
using System.Collections.Generic;

public abstract class Character
{
    public string name;
    public int age;
    public TotalBody totalBody;

    public int BirthMonth { get; protected set; }

    public int Age
    {
        get
        {
            return this.age + ((Manager.Game.Protagonist.CurrentTurnProtagonist + this.BirthMonth) / 12);
        }
    }

    public Characteristics characteristics = new Characteristics();
    
    public Character(string name, int age)
    {
        this.name = name;
        this.age = age;
        this.BirthMonth = Random.Range(0, 12);
    }

    public void GenerateBody()
    {
        int targetBeauty = this.characteristics.Beauty;
        this.totalBody = TotalBody.GenerateBodyWithBeauty(this);
        this.UpdateBody();
    }

    public void UpdateBody()
    {
        this.totalBody.SetAppearance(Manager.CharactersVisuals.GetBodyFromFitness(this), TotalBody.Name.Body);
    }

    public abstract void NextTurn();
}