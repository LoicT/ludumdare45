using UnityEngine;

public class CharacterFactory
{
    public static Target GenerateTargetFromPrestige(float prestige)
    {
        prestige = Mathf.Clamp(prestige, 0, Characteristics.MAX_PRESTIGE);
        Target res = new Target();

        float[] values = new float[Characteristics.NB_STATS];
        float total = 0;
        for (int i = 0; i < values.Length; i++)
        {
            values[i] = Random.Range(0f, 1f);
            total += values[i];
        }
        int j = 0;
        foreach (Characteristics.Name name in System.Enum.GetValues(typeof(Characteristics.Name)))
        {
            res.characteristics.SetCharacteristicValue(name, Mathf.CeilToInt((values[j] * (prestige) / total) * (Characteristics.NB_STATS / (float)Characteristics.MAX_PRESTIGE) * Characteristics.MAX_STAT_VALUE));
            j++;
        }
        CharacterFactory.AddPreferencesFromPrestige(res, prestige);
        res.GenerateBody();
        return res;
    }

    private static void AddPreferencesFromPrestige(Target target, float prestige)
    {
        if (prestige < Characteristics.MAX_PRESTIGE / 5)
        {
            return;
        }
        if (prestige >= Characteristics.MAX_PRESTIGE / 5)
        {
            target.preferences.Add(GenerateRandomPreference(prestige));
        }
        if (prestige >= Characteristics.MAX_PRESTIGE / 5 * 3)
        {
            target.preferences.Add(GenerateRandomPreference(prestige));
        }
        if (prestige == Characteristics.MAX_PRESTIGE)
        {
            target.preferences.Add(GenerateRandomPreference(prestige));
        }
    }

    private static Preference GenerateRandomPreference(float prestige)
    {
        int typePreference = Random.Range(0, 3);
        switch (typePreference)
        {
            case 0:
                return CharacteristicPreference.GeneratePreference(prestige);
            case 1:
            //TODO: Item preference
            //return ItemPreference.GeneratePreference(prestige);
            case 2:
                return MoneyPreference.GeneratePreference(prestige);
            default:
                throw new System.Exception("AH SHIT I'M IN TROUBLE !");
        }
    }

}