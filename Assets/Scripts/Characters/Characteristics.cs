using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class Characteristics
{
    public enum Name
    {
        Beauty, Fitness, Intelligence
    }

    public static int NB_STATS
    {
        get
        {
            return Enum.GetValues(typeof(Name)).Length;
        }
    }

    public static readonly float MAX_STAT_VALUE = 100;

    public static readonly int MAX_PRESTIGE = 5;

    private List<int> stats = new List<int>(new int[NB_STATS]);

    private void SetStat(Name name, int value)
    {
        this.stats[(int)name] = (int)Mathf.Clamp(value, 0, MAX_STAT_VALUE);
    }

    public int Beauty
    {
        get
        {
            return this.stats[(int)Name.Beauty];
        }
        set
        {
            this.SetStat(Name.Beauty, value);
        }
    }

    public int Fitness
    {
        get
        {
            return this.stats[(int)Name.Fitness];
        }
        set
        {
            this.SetStat(Name.Fitness, value);
        }
    }

    public int Intelligence
    {
        get
        {
            return this.stats[(int)Name.Intelligence];
        }
        set
        {
            this.SetStat(Name.Intelligence, value);
        }
    }

    /// Return the prestige as a mark from 0 to 5
    public float Prestige
    {
        get
        {
            return (this.stats.Aggregate(0f, (float acc, int stat) =>
            {
                acc = acc + (stat / (float)MAX_STAT_VALUE);
                return acc;
            }) / NB_STATS) * MAX_PRESTIGE;
        }
    }

    public int GetCharacteristicValue(Name characteristics)
    {
        return this.stats[(int)characteristics];
    }

    public void SetCharacteristicValue(Name characteristic, int value)
    {
        this.SetStat(characteristic, value);
    }

    public static Characteristics MixCharacteristics(Characteristics characteristics1, Characteristics characteristics2)
    {
        System.Random random = new System.Random();
        Characteristics result = new Characteristics();
        foreach (Name name in Enum.GetValues(typeof(Name)))
        {
            int randomValue = random.Next(0, 11);
            int min = Mathf.Min(characteristics1.GetCharacteristicValue(name), characteristics2.GetCharacteristicValue(name));
            int max = Mathf.Max(characteristics1.GetCharacteristicValue(name), characteristics2.GetCharacteristicValue(name));
            result.SetCharacteristicValue(name, random.Next(min, max + 1) + randomValue - 5);
        }
        return result;
    }

}