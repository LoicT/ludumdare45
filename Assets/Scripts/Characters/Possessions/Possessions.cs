﻿
using System.Collections.Generic;
using UnityEngine;

public class Possessions
{
    public static readonly float INHERITANCE_PERCENTAGE = 0.4f;

    public static Possessions Inherit(Protagonist protagonist, Target target)
    {
        Possessions result = new Possessions();
        result.items.UnionWith(protagonist.possessions.items);
        protagonist.possessions.Money -= (int) (protagonist.possessions.Money * INHERITANCE_PERCENTAGE);
        result.money = (int) (protagonist.possessions.Money  * INHERITANCE_PERCENTAGE);
        return result;
    }

    private int money;
    public int Money
    {
        get
        {
            return this.money;
        }

        set
        {
            this.money = Mathf.Max(0, value);
        }
    }

    public HashSet<Item> items = new HashSet<Item>();

    public bool HasItem(Item item)
    {
        return this.items.Contains(item);
    }

    public bool HasVehicle()
    {
        foreach (Item item in this.items)
        {
            if (item as Vehicle != null)
            {
                return true;
            }
        }
        return false;
    }
}
