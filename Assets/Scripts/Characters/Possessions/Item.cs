﻿using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Item/Common", order = 0)]
public class Item : ScriptableObject
{
    public new string name;
    public int cost = 10;

    public bool CanBuy()
    {
        return Manager.Game.Protagonist.possessions.Money >= this.cost;
    }
}
