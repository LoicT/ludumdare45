using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Protagonist : Character
{
    public const int STARTING_AGE = 18;
    private int startingTurn;

    public Profession Job { get; set; }

    public Target mother;

    public int CurrentTurnProtagonist
    {
        get
        {
            return Manager.Game.CurrentTurn - this.startingTurn;
        }
    }

    public static readonly int BASE_ACTION_POINTS = 10;
    public int actionPointsInProfession = 0;

    private int actionPoints;
    public int ActionPoints
    {
        get
        {
            return this.actionPoints;
        }

        set
        {
            this.actionPoints = Mathf.Max(0, value);
            Manager.UI.ShowProtagonist();
        }
    }

    public Possessions possessions = new Possessions();

    public List<Target> Acquaintances { get; set; }

    public Protagonist(string name, int startingTurn) : base(name, STARTING_AGE)
    {
        this.startingTurn = startingTurn;
        this.Acquaintances = new List<Target>();
        this.BirthMonth = 0;
    }

    public override void NextTurn()
    {
        this.ActionPoints = Protagonist.BASE_ACTION_POINTS;

        foreach (Target target in this.Acquaintances)
            target.NextTurn();

        bool randomEvent = false;
        if (this.Job != null)
        {
            this.JobTurn(ref randomEvent);
        }

        if (!randomEvent)
        {
            Manager.RandomEvents.ShowRandomEvent(Manager.RandomEvents.GetRandomEvent(this));
        }
    }

    private void JobTurn(ref bool randomEvent)
    {
        this.ActionPoints -= this.actionPointsInProfession;
        this.possessions.Money += this.Job.salary * this.actionPointsInProfession;
        if (this.actionPointsInProfession < this.Job.minimumActionPoints)
        {
            if (Random.Range(0, 100) <= Manager.Professions.firedEvent.probability)
            {
                randomEvent = true;
                Manager.RandomEvents.ShowRandomEvent(Manager.Professions.firedEvent);
            }
        }
    }

    public void PerformAction(Action action, object parameter = null)
    {
        if (!action.IsPossible())
            throw new System.Exception("This action is not possible");
        action.PerformAction(parameter);
    }

    public Protagonist GiveBirth(Target target)
    {
        Characteristics nextCharacteristics = Characteristics.MixCharacteristics(target.characteristics, this.characteristics);
        Protagonist nextProtagonist = new Protagonist("Child", Manager.Game.CurrentTurn);
        nextProtagonist.characteristics = nextCharacteristics;
        nextProtagonist.possessions = Possessions.Inherit(this, target);
        nextProtagonist.GenerateBody();
        nextProtagonist.totalBody.Body.color = Random.Range(0, 2) == 1 ? this.totalBody.Body.color : target.totalBody.Body.color;
        nextProtagonist.mother = target;
        target.StopPregnancy();
        return nextProtagonist;
    }

    public void AddPointToProfession()
    {
        if (this.actionPoints > 0)
        {
            this.actionPointsInProfession++;
            this.ActionPoints--;
        }
    }

    public void RemovePointToProfession()
    {
        if (this.actionPointsInProfession > 0)
        {
            this.actionPointsInProfession--;
            this.ActionPoints++;
        }
    }

    public void Buy(Item item)
    {
        if (!item.CanBuy())
            throw new System.Exception("You don't have enough money");
        this.possessions.Money -= item.cost;
        this.possessions.items.Add(item);
        Manager.UI.ShowProtagonist();
    }

    public override string ToString()
    {
        string s = "";
        s += "Beauty: " + this.characteristics.Beauty + "\n";
        s += "Fitness: " + this.characteristics.Fitness + "\n";
        s += "Intelligence: " + this.characteristics.Intelligence + "\n";
        return s;
    }
}
