using System.Collections.Generic;
using System;
using UnityEngine;

public class ItemPreference : Preference
{
    public Item item;

    public ItemPreference(Item item)
    {
        this.item = item;
    }

    public override bool ArePreferencesMet()
    {
        return Manager.Game.Protagonist.possessions.HasItem(item);
    }

    public override int Deficit()
    {
        return this.ArePreferencesMet() ? 0 : 1;
    }

    public static ItemPreference GeneratePreference(float prestige)
    {
        throw new System.NotImplementedException();
    }
}