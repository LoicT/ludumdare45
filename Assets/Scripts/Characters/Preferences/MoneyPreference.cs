using UnityEngine;

public class MoneyPreference : Preference
{
    public int quantity = 1;

    public MoneyPreference(int quantity)
    {
        this.quantity = quantity;
    }

    public override bool ArePreferencesMet()
    {
        return Manager.Game.Protagonist.possessions.Money >= this.quantity;
    }

    public override int Deficit()
    {
        return this.ArePreferencesMet() ? 0 : this.quantity -= Manager.Game.Protagonist.possessions.Money;
    }

    public static MoneyPreference GeneratePreference(float prestige)
    {
        //TODO: Define the right amount of money for the given prestige
        return new MoneyPreference((int) (prestige / Characteristics.MAX_PRESTIGE * (Characteristics.MAX_STAT_VALUE - 10) + (Random.Range(0, 11) - 5)));
    }
}