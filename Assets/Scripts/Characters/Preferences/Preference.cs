using UnityEngine;

public abstract class Preference
{
    public abstract bool ArePreferencesMet();

    public abstract int Deficit();
}