using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacteristicPreference : Preference
{
    public Characteristics.Name characteristic;
    private int quantity;
    public int Quantity
    {
        get
        {
            return this.quantity;
        }
        set
        {
            this.quantity = (int)Mathf.Clamp(value, 0, Characteristics.MAX_STAT_VALUE);
        }
    }

    public CharacteristicPreference(Characteristics.Name characteristic, int quantity)
    {
        this.characteristic = characteristic;
        this.quantity = quantity;
    }


    public override bool ArePreferencesMet()
    {
        if (Manager.Game.Protagonist.characteristics.GetCharacteristicValue(this.characteristic) < this.Quantity)
        {
            return false;
        }
        return true;
    }

    public override int Deficit()
    {
        return this.quantity - Manager.Game.Protagonist.characteristics.GetCharacteristicValue(this.characteristic);
    }

    public static CharacteristicPreference GeneratePreference(float prestige)
    {
        System.Array values = System.Enum.GetValues(typeof(Characteristics.Name));
        int randomValue = Random.Range(0, values.Length);
        return new CharacteristicPreference(
            (Characteristics.Name)values.GetValue(randomValue),
                (int)(prestige / Characteristics.MAX_PRESTIGE * (Characteristics.MAX_STAT_VALUE - 10) + (Random.Range(0, 11) - 5)));
    }
}