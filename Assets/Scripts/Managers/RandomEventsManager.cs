﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class RandomEventsManager : MonoBehaviour
{
    public List<RandomEvent> randomEvents;

    private RandomEvent currentEvent;

    private class RandomEventHistory
    {
        public RandomEvent randomEvent;
        public int cooldown;

        public RandomEventHistory(RandomEvent e)
        {
            this.randomEvent = e;
            this.cooldown = e.cooldown;
        }
    }

    private List<RandomEventHistory> history = new List<RandomEventHistory>();

    public List<RandomEvent> PossibleEvents()
    {
        return this.randomEvents.FindAll(x => x.CanOccur());
    }

    public int TotalProbability()
    {
        return this.PossibleEvents().Aggregate(0, (acc, randomEvent) => acc += randomEvent.probability);
    }

    public void ShowRandomEvent(RandomEvent randomEvent)
    {
        this.currentEvent = randomEvent;

        if (!string.IsNullOrEmpty(randomEvent.Message()))
        {
            Manager.UI.randomEventScrollrect.normalizedPosition = new Vector2(0, 1);

            foreach (Transform child in Manager.UI.randomEventChoicesRoot)
                Destroy(child.gameObject);

            Manager.UI.randomEventMessage.text = this.currentEvent.Message();

            Manager.UI.ClearChoices();
            if (this.currentEvent.choices.Count > 0)
            {
                for (int i = 0; i < this.currentEvent.choices.Count; i++)
                {
                    int choice = i;
                    Manager.UI.InitializeChoice(Manager.UI.randomEventChoicesRoot, this.currentEvent.choices[i], delegate { this.ActOnRandomEvent(choice); });
                }
            }
            else
            {
                Manager.UI.InitializeChoice(Manager.UI.randomEventChoicesRoot, "OK", delegate { this.ActOnRandomEvent(-1); });
            }

            Manager.UI.randomEventsBackground.SetActive(true);
            Manager.UI.randomEvent.SetActive(true);

            if (randomEvent.cooldown != 0)
            {
                this.history.Add(new RandomEventHistory(randomEvent));
            }
        }
        else
        {
            this.ActOnRandomEvent(-1);
        }
    }

    public void ActOnRandomEvent(int choice)
    {
        RandomEvent randomEvent = this.currentEvent;
        Manager.UI.randomEventsBackground.SetActive(false);
        Manager.UI.randomEvent.SetActive(false);
        this.currentEvent = null;

        randomEvent.Resolve(choice);
    }

    public RandomEvent GetRandomEvent(Protagonist protagonist)
    {
        bool ok = false;
        int i = -1;
        do
        {
            int totalProbability = this.TotalProbability();
            int number = Random.Range(1, totalProbability);
            i = -1;
            while (number > 0)
            {
                i++;
                if (i >= this.PossibleEvents().Count)
                    throw new System.Exception("No more random event found");
                number -= this.PossibleEvents()[i].probability;
            }

            if (this.history.Find(x => x.randomEvent == this.PossibleEvents()[i]) == null)
                ok = true;
        } while (!ok);

        return this.PossibleEvents()[i];
    }

    public void UpdateHistory()
    {
        foreach (RandomEventHistory h in this.history)
        {
            h.cooldown--;
        }

        this.history.RemoveAll(x => x.cooldown <= 0);
    }
}
