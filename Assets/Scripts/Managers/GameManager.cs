﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static readonly int TIME_LIMIT = 40;

    public int CurrentTurn = 1;

    public List<Protagonist> Lineage { get; private set; }

    public Protagonist Protagonist
    {
        get
        {
            return this.Lineage[this.Lineage.Count - 1];
        }
    }

    private void Awake()
    {
        this.StartGame();
    }

    public void StartGame()
    {
        this.CurrentTurn = 1;
        this.Lineage = new List<Protagonist>();
        this.GenerateProtagonist();
        this.UpdateTimeLimit();
        Manager.Professions.ResetJobs();
        Manager.Name.ResetNamesList();
        Manager.UI.CloseApp();
        Manager.UI.lineageUI.SetActive(false);
    }

    public void GenerateProtagonist()
    {
        Protagonist protagonist = new Protagonist("Bobby", this.CurrentTurn);
        protagonist.GenerateBody();
        this.AddToLineage(protagonist);
    }

    public void AddToLineage(Protagonist protagonist)
    {
        this.Lineage.Add(protagonist);
        Manager.Professions.ResetJobs();
        protagonist.ActionPoints = Protagonist.BASE_ACTION_POINTS;
        this.UpdateTimeLimit();
    }

    public void NextTurn()
    {
        Manager.UI.CloseApp();
        this.CurrentTurn++;
        Manager.RandomEvents.UpdateHistory();
        this.Protagonist.NextTurn();
        Manager.UI.ShowProtagonist();
        this.UpdateTimeLimit();
    }

    private void UpdateTimeLimit()
    {
        int timeLeft = TIME_LIMIT - this.Protagonist.CurrentTurnProtagonist;
        if (timeLeft <= 0)
        {
            this.GameOver();
        }
        else
        {
            Manager.UI.timeLeft.text = timeLeft == 1 ? "One month left!" : timeLeft + " months left...";
        }
    }

    public void GameOver()
    {
        Manager.UI.lineageUI.SetActive(true);
        Manager.UI.lineageUI.GetComponentInChildren<LineageManager>().Initialize();
    }
}
