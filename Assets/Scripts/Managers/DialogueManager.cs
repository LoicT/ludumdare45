﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DialogueManager : MonoBehaviour
{
    public enum Setting { Phone, Club, Park };

    [System.Serializable]
    public class Conversation
    {
        public Setting setting;
        public bool hasMet;
        public bool girlInitiative;
        public Message firstMessage;
    }

    public Conversation[] conversations;

    private List<Conversation> unseenConversations = new List<Conversation>();

    public int LastChoice { get; private set; }
    private Message currentMessage;
    public Target interlocutor { get; private set; }


    public Message GenerateConversation(Setting setting, bool hasMet, bool girlInitiative)
    {
        List<Conversation> conversations = this.ConversationsBySetting(setting, hasMet, girlInitiative);
        if (conversations.Count == 0)
        {
            this.ResetConversationsList(setting, hasMet, girlInitiative);
            conversations = this.ConversationsBySetting(setting, hasMet, girlInitiative);
        }

        // Get a random conversation that hasn't been seen by the player yet
        int randomValue = Random.Range(0, conversations.Count);
        Message message = conversations[randomValue].firstMessage;
        this.unseenConversations.Remove(conversations[randomValue]);

        return message;
    }

    private List<Conversation> ConversationsBySetting(Setting setting, bool hasMet, bool girlInitiative)
    {
        return this.unseenConversations.FindAll(x => x.setting == setting && x.hasMet == hasMet && x.girlInitiative == girlInitiative);
    }

    public void ResetConversationsList(Setting setting, bool hasMet, bool girlInitiative)
    {
        this.unseenConversations.AddRange(this.conversations.ToList().FindAll(x => x.setting == setting && x.hasMet == hasMet && x.girlInitiative == girlInitiative));
    }




    public void StartDialogue(Target interlocutor, Message message)
    {
        this.interlocutor = interlocutor;
        this.interlocutor.LastInteraction = Manager.Game.CurrentTurn;

        Manager.UI.CloseApp();
        this.ShowInterlocutor();
        Manager.UI.dialogueUI.SetActive(true);
        Manager.UI.mainUI.SetActive(false);

        this.currentMessage = message;
        this.currentMessage.Consequences();
        this.ShowMessage();
    }

    private void ShowMessage()
    {
        this.LastChoice = -1;

        Manager.UI.dialogueBox.text = this.currentMessage.Text;
        Manager.UI.ClearChoices();
        if (this.currentMessage.Choices.Count > 0)
        {
            for (int i = 0; i < this.currentMessage.choices.Count; i++)
            {
                if (this.currentMessage.choices[i].AreAllConditionsFulfilled())
                {
                    int choice = i;
                    Manager.UI.InitializeChoice(Manager.UI.choicesRoot, this.currentMessage.choices[i].choice, delegate { this.PickChoice(choice); });
                }
            }
        }
        else
        {
            Manager.UI.InitializeChoice(Manager.UI.choicesRoot, this.currentMessage.GetNextMessage() == null ? "End Conversation" : "Next", delegate { this.ContinueDialogue(); });
        }
    }

    private void EndDialogue()
    {
        this.currentMessage = null;

        if (!Manager.Game.Protagonist.Acquaintances.Contains(this.interlocutor))
            Manager.Game.Protagonist.Acquaintances.Add(this.interlocutor);

        Manager.UI.dialogueUI.SetActive(false);
        Manager.UI.mainUI.SetActive(true);
    }

    public void ContinueDialogue()
    {
        this.currentMessage = this.currentMessage.GetNextMessage();
        if (this.currentMessage == null)
        {
            this.EndDialogue();
        }
        else
        {
            this.currentMessage.Consequences();
            this.ShowMessage();
        }
    }

    public void PickChoice(int choice)
    {
        this.LastChoice = choice;
        this.ContinueDialogue();
    }


    public void ShowInterlocutor()
    {
        Manager.UI.targetPanel.Initialize(this.interlocutor);
    }
}
