using UnityEngine;
using System.Collections.Generic;

public class ProfessionManager : MonoBehaviour
{
    [System.Serializable]
    public class Job
    {
        private static readonly int TIME_BEFORE_AVAILABILITY = 5;

        public Profession profession;
        private int lastTimeQuit = -1;

        public void Quit()
        {
            this.lastTimeQuit = Manager.Game.CurrentTurn;
        }

        public bool Available()
        {
            return this.lastTimeQuit == -1 || Manager.Game.CurrentTurn - this.lastTimeQuit > TIME_BEFORE_AVAILABILITY;
        }
        
        public void Reset()
        {
            this.lastTimeQuit = -1;
        }
    }

    public RandomEvent firedEvent;

    public List<Job> jobs;

    public List<Profession> AvailableProfessions()
    {
        List<Profession> professions = new List<Profession>();
        foreach (Job job in this.jobs)
        {
            if (job.Available())
                professions.Add(job.profession);
        }

        return professions;
    }

    public void QuitProfession()
    {
        while (Manager.Game.Protagonist.actionPointsInProfession > 0)
            Manager.Game.Protagonist.RemovePointToProfession();

        this.jobs.Find(x => x.profession == Manager.Game.Protagonist.Job).Quit();

        Manager.Game.Protagonist.Job = null;
    }

    public void ResetJobs()
    {
        foreach (Job job in this.jobs)
        {
            job.Reset();
        }
    }
}