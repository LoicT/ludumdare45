﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CharactersVisualsManager : MonoBehaviour
{
    [System.Serializable]
    public class BodyPartVisuals
    {
        public TotalBody.Name part;
        public Color[] colors;
        public Appearance[] appearances;

        public Color RandomColor()
        {
            return this.colors[Random.Range(0, this.colors.Length)];
        }

        public Appearance RandomAppearance(int targetBeauty)
        {
            List<Appearance> possibleAppearances = this.GetNearestAppearences(targetBeauty);
            return possibleAppearances[Random.Range(0, possibleAppearances.Count)];
        }

        private List<Appearance> GetNearestAppearences(int targetBeauty)
        {
            List<Appearance> res = new List<Appearance>();
            int maxDistance = Mathf.Abs(targetBeauty - this.appearances[0].beauty);
            foreach (Appearance appearance in this.appearances)
            {
                int distance = Mathf.Abs(targetBeauty - appearance.beauty);
                if (distance < maxDistance)
                {
                    res = new List<Appearance>();
                    res.Add(appearance);
                }
                else if (distance == maxDistance)
                {
                    res.Add(appearance);
                }
            }
            return res;
        }
    }

    [System.Serializable]
    public class BodyShape
    {
        public int minFitness;
        public Appearance appearance;
    }

    [FoldoutGroup("Protagonists")]
    public List<BodyPartVisuals> protagonistsVisuals;

    [FoldoutGroup("Targets")]
    public List<BodyPartVisuals> targetsVisuals;

    [FoldoutGroup("Protagonists")]
    public List<BodyShape> protagonistsBodies;

    [FoldoutGroup("Targets")]
    public List<BodyShape> targetsBodies;

    public BodyPartVisuals GetBodyPartVisualFromName(TotalBody.Name name, bool target)
    {
        List<BodyPartVisuals> list = target ? this.targetsVisuals : this.protagonistsVisuals;
        return list.Find((BodyPartVisuals bodyPartVisuals) => bodyPartVisuals.part == name);
    }

    public Appearance GetBodyFromFitness(Character character)
    {
        List<BodyShape> list = character is Target ? this.targetsBodies : this.protagonistsBodies;
        return list.Find(x => character.characteristics.Fitness >= x.minFitness).appearance;
    }
}
