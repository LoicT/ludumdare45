using UnityEngine;
using System.Collections.Generic;

public class TargetNameManager : MonoBehaviour
{
    private static readonly string[] names = new string[] {
        "Emily",
        "Megan",
        "Jessica",
        "Hannah",
        "Sarah",
        "Rebecca",
        "Eliza",
        "Victoria",
        "Sam",
        "Alex",
        "Anna",
        "Olivia",
        "Jennifer",
        "Katie",
        "Amber",
        "Caitlin",
        "Jasmine",
        "Abigail",
        "Melissa",
        "Laura",
        "Danielle",
        "Molly",
        "Nicole",
        "Steph",
        "Amy",
        "Rachel",
        "Chelsea",
        "Lisa",
        "Alicia",
        "Mary",
        "Natasha",
        "Paige",
        "Leah",
        "Jenna",
        "Emma",
        "Tiffany",
        "Charlotte",
        "Sophie",
        "Chloe",
        "Lauren",
        "Eleanor",
        "Lucy",
        "Kelly",
        "Alice",
        "Michelle",
        "Zoe",
        "Grace",
        "Ella",
        "Amanda",
        "Tara",
        "Nadia"
    };

    private List<string> unusedNames = new List<string>();
    
    public string GetName()
    {
        if (this.unusedNames.Count == 0)
        {
            this.ResetNamesList();
        }
        
        int randomValue = Random.Range(0, this.unusedNames.Count);
        string name = this.unusedNames[randomValue];
        this.unusedNames.RemoveAt(randomValue);
        return name;
    }

    public int GetAge(int min = 18, int max = 25)
    {
        return Random.Range(min, max);
    }

    public void ResetNamesList()
    {
        this.unusedNames = new List<string>(TargetNameManager.names);
    }
}