using UnityEngine;

public class LineageManager : MonoBehaviour
{
    public void Initialize()
    {
        Manager.UI.lineageLengthText.text = "You played for " + Manager.Game.Lineage.Count + (Manager.Game.Lineage.Count > 1 ? " generations" : " generation");

        foreach (Transform child in Manager.UI.coupleUIRoot)
            Destroy(child.gameObject);

        for (int i = 0; i < Manager.Game.Lineage.Count; i++)
        {
            CouplePanel couplePanel = Instantiate(Manager.UI.couplePrefab, Manager.UI.coupleUIRoot).GetComponent<CouplePanel>();
            Target target = i < Manager.Game.Lineage.Count - 1 ? Manager.Game.Lineage[i + 1].mother : null;
            couplePanel.Initialize(Manager.Game.Lineage[i], target);
        }
    }
}