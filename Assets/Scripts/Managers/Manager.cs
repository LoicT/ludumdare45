﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Manager
{
    public static GameManager Game;
    public static UIManager UI;
    public static RandomEventsManager RandomEvents;
    public static CharactersVisualsManager CharactersVisuals;
    public static TargetNameManager Name;
    public static ProfessionManager Professions;
    public static ItemManager Item;
    public static DialogueManager Dialogue;

    static Manager()
    {
        GameObject system = GameObject.Find("SYSTEM");
        Game = system.GetComponentInChildren<GameManager>();
        UI = system.GetComponentInChildren<UIManager>();
        RandomEvents = system.GetComponentInChildren<RandomEventsManager>();
        CharactersVisuals = system.GetComponentInChildren<CharactersVisualsManager>();
        Professions = system.GetComponentInChildren<ProfessionManager>();
        Name = system.GetComponentInChildren<TargetNameManager>();
        Item = system.GetComponentInChildren<ItemManager>();
        Dialogue = system.GetComponentInChildren<DialogueManager>();
    }

}
