﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{
    public delegate void ChoiceDelegate();

    [FoldoutGroup("Main UI")]
    public GameObject mainUI;

    [FoldoutGroup("Main UI")]
    public GameObject dialogueUI;

    [FoldoutGroup("Main UI")]
    public GameObject lineageUI;

    [FoldoutGroup("Main UI")]
    public Text timeLeft;

    [FoldoutGroup("Apps UI")]
    public GameObject appsUIObject;

    [FoldoutGroup("Apps UI")]
    public RectTransform appsUIRoot;

    [FoldoutGroup("Apps UI")]
    public GameObject appsInfoText;

    [FoldoutGroup("Dialogue")]
    public TargetPanel targetPanel;

    [FoldoutGroup("Dialogue")]
    public Text dialogueBox;

    [FoldoutGroup("Dialogue")]
    public GameObject choicePrefab;

    [FoldoutGroup("Dialogue")]
    public RectTransform choicesRoot;

    [FoldoutGroup("Random Event")]
    public GameObject randomEvent;

    [FoldoutGroup("Random Event")]
    public Text randomEventMessage;

    [FoldoutGroup("Random Event")]
    public RectTransform randomEventChoicesRoot;

    [FoldoutGroup("Random Event")]
    public GameObject randomEventsBackground;

    [FoldoutGroup("Random Event")]
    public ScrollRect randomEventScrollrect;


    [FoldoutGroup("Actions")]
    public GameObject actionPrefab;

    [FoldoutGroup("Lineage")]
    public GameObject couplePrefab;

    [FoldoutGroup("Lineage")]
    public RectTransform coupleUIRoot;

    [FoldoutGroup("Lineage")]
    public Text lineageLengthText;

    [FoldoutGroup("Items")]
    public GameObject itemPrefab;

    [FoldoutGroup("Beauty Parlor")]
    public GameObject beautyParlorPrefab;

    [FoldoutGroup("Job")]
    public GameObject jobPrefab;

    [FoldoutGroup("Job")]
    public GameObject jobInfoPrefab;

    [FoldoutGroup("Stalkedex")]
    public GameObject targetPrefab;

    [FoldoutGroup("Stalkedex")]
    public Gradient affinityColors;


    [FoldoutGroup("Gauges")]
    public GameObject gaugePrefab;

    [FoldoutGroup("Gauges")]
    public RectTransform gaugesRoot;


    [FoldoutGroup("Protagonist")]
    public Image prBody;

    [FoldoutGroup("Protagonist")]
    public Image prMouth;

    [FoldoutGroup("Protagonist")]
    public Image prEyes;

    [FoldoutGroup("Protagonist")]
    public Image prBrows;

    [FoldoutGroup("Protagonist")]
    public Image prHairBack;

    [FoldoutGroup("Protagonist")]
    public Image prHair;

    [FoldoutGroup("Protagonist")]
    public Image prClothes;

    [FoldoutGroup("Protagonist")]
    public InputField prName;

    [FoldoutGroup("Protagonist")]
    public Text prAge;

    [FoldoutGroup("Protagonist")]
    public Text prMoney;

    [FoldoutGroup("Protagonist")]
    public Text prSalary;

    [FoldoutGroup("Protagonist")]
    public Text prAP;

    private App currentApp;
    private Dictionary<Characteristics.Name, Slider> gauges;

    private void Start()
    {
        this.appsUIObject.SetActive(false);
        this.randomEvent.SetActive(false);
        this.randomEventsBackground.SetActive(false);
        this.gauges = this.InitializeGauges(Manager.Game.Protagonist, this.gaugesRoot);
        this.ShowProtagonist();
    }

    public Dictionary<Characteristics.Name, Slider> InitializeGauges(Character character, RectTransform root)
    {
        Dictionary<Characteristics.Name, Slider> gauges = new Dictionary<Characteristics.Name, Slider>();
        foreach (Characteristics.Name characteristic in Enum.GetValues(typeof(Characteristics.Name)))
        {
            GameObject gauge = Instantiate(this.gaugePrefab, root);
            gauge.GetComponentInChildren<Text>().text = characteristic.ToString();
            gauges.Add(characteristic, gauge.GetComponentInChildren<Slider>());
        }

        this.UpdateGauges(character, gauges);
        return gauges;
    }

    private void UpdateGauges(Character character, Dictionary<Characteristics.Name, Slider> gauges)
    {
        foreach (KeyValuePair<Characteristics.Name, Slider> gauge in gauges)
        {
            gauge.Value.value = character.characteristics.GetCharacteristicValue(gauge.Key) / Characteristics.MAX_STAT_VALUE;
        }
    }

    public void OpenApp(App app)
    {
        this.CloseApp();
        app.Open();
        this.currentApp = app;
    }

    public void UpdateApp()
    {
        if (this.currentApp != null)
        {
            this.OpenApp(this.currentApp);
        }

    }

    public void CloseApp()
    {
        this.currentApp = null;

        foreach (Transform child in this.appsUIRoot)
            Destroy(child.gameObject);

        Manager.UI.appsUIRoot.DetachChildren();

        Manager.UI.appsUIObject.SetActive(false);
    }

    public void ShowProtagonist()
    {
        this.ShowBodyPart(this.prBody, Manager.Game.Protagonist.totalBody.Body, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prMouth, Manager.Game.Protagonist.totalBody.Mouth, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prEyes, Manager.Game.Protagonist.totalBody.Eyes, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prBrows, Manager.Game.Protagonist.totalBody.Brows, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prHairBack, Manager.Game.Protagonist.totalBody.HairBack, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prHair, Manager.Game.Protagonist.totalBody.Hair, Manager.Game.Protagonist);
        this.ShowBodyPart(this.prClothes, Manager.Game.Protagonist.totalBody.Clothes, Manager.Game.Protagonist);
        this.prName.text = Manager.Game.Protagonist.name;
        this.prAge.text = Manager.Game.Protagonist.Age.ToString();
        this.prMoney.text = "<b>Money:</b> " + Manager.Game.Protagonist.possessions.Money + "$";
        this.prAP.text = Manager.Game.Protagonist.ActionPoints.ToString();
        this.prSalary.text = Manager.Game.Protagonist.Job != null ? "<b>Salary:</b> " + (Manager.Game.Protagonist.Job.salary * Manager.Game.Protagonist.actionPointsInProfession).ToString() + "$" : "";

        if (this.gauges != null)
            this.UpdateGauges(Manager.Game.Protagonist, this.gauges);
    }

    public void ShowBodyPart(Image image, BodyPart part, Character character)
    {
        image.sprite = part.appearance.Sprite(character);
        image.color = part.Color();
    }


    public void ClearChoices()
    {
        foreach (Transform child in this.choicesRoot)
            Destroy(child.gameObject);
    }

    public void InitializeChoice(RectTransform root, string text, ChoiceDelegate function)
    {
        GameObject button = Instantiate(this.choicePrefab, root);
        button.GetComponentInChildren<Text>().text = text;

        button.GetComponent<Button>().onClick.AddListener(delegate { function(); });
    }

    public void ChangeProtagonistName()
    {
        Manager.Game.Protagonist.name = this.prName.text;
    }
}
