using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ItemManager : MonoBehaviour
{
    public List<Item> items;

    public HashSet<Item> GetBuyableItems()
    {
        HashSet<Item> res = new HashSet<Item>(this.items);
        res.ExceptWith(Manager.Game.Protagonist.possessions.items);
        return res;
    }

}