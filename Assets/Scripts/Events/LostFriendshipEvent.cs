﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Lost Friendship", menuName = "Random Event/Lost Friendship")]
public class LostFriendshipEvent : RandomEvent
{
    public int minAffinityForMessage = 50;

    public override bool CanOccur()
    {
        return this.Contact() != null;
    }

    public override void Resolve(int choice)
    {
        Manager.Game.Protagonist.Acquaintances.Remove(this.Contact());
    }

    private Target Contact()
    {
        return Manager.Game.Protagonist.Acquaintances.Find(x => Manager.Game.CurrentTurn - x.LastInteraction > Target.TIME_BEFORE_FORGETTING && !x.Pregnant);
    }

    public override string Message()
    {
        if (this.Contact().Affinity < minAffinityForMessage)
            return string.Empty;
        else
            return this.message.Replace("{target}", this.Contact().name);
    }
}
