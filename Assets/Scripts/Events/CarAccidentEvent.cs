﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Car Accident", menuName = "Random Event/Car Accident")]
public class CarAccidentEvent : RandomEvent
{
    public int severity = 8;

    public override bool CanOccur()
    {
        return Manager.Game.Protagonist.possessions.HasVehicle();
    }

    public override void Resolve(int choice)
    {
        Manager.Game.Protagonist.ActionPoints -= severity;
        if (Manager.Game.Protagonist.actionPointsInProfession > Manager.Game.Protagonist.ActionPoints)
        {
            Manager.Game.Protagonist.actionPointsInProfession = Manager.Game.Protagonist.ActionPoints;
            Manager.Game.Protagonist.ActionPoints = 0;
        }
    }
}