﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pregnant", menuName = "Random Event/Pregnant")]
public class PregnantEvent : RandomEvent
{
    public float minAffinity = 90;

    public override bool CanOccur()
    {
        return this.PregnantGirl() != null;
    }

    public override void Resolve(int choice)
    {
        Target target = this.PregnantGirl();
        target.GetPregnant();
    }

    public override string Message()
    {
        return this.message.Replace("{target}", this.PregnantGirl().name);
    }

    private Target PregnantGirl()
    {
        return Manager.Game.Protagonist.Acquaintances.Find(x => x.Affinity >= minAffinity && !x.Pregnant);
    }
}