using UnityEngine;

[CreateAssetMenu(fileName = "No Event", menuName = "Random Event/No Event")]
public class NoRandomEvent : RandomEvent
{
    public override bool CanOccur()
    {
        return true;
    }

    public override void Resolve(int choice)
    {

    }
}