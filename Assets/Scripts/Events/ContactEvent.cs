﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Contact", menuName = "Random Event/Contact")]
public class ContactEvent : RandomEvent
{
    public int minAffinity = 50;
    public int affinityReductionOnRefusal = 5;
    public DialogueManager.Setting setting;

    public static Target CurrentContact;

    public override bool CanOccur()
    {
        return this.Contact() != null; 
    }

    public override void Resolve(int choice)
    {
        switch (choice)
        {
            case 0:
                Manager.Dialogue.StartDialogue(this.Contact(), Manager.Dialogue.GenerateConversation(setting, true, true));
                break;
            default:
                CurrentContact.AddAffinity(-this.affinityReductionOnRefusal);
                break;
        }

        CurrentContact = null;
    }

    public override string Message()
    {
        if (CurrentContact == null)
            CurrentContact = this.Contact();

        return this.message.Replace("{target}", CurrentContact.name);
    }

    private Target Contact()
    {
        List<Target> possibleContacts = Manager.Game.Protagonist.Acquaintances.FindAll(x => x.Affinity >= this.minAffinity);
        return possibleContacts.Count > 0 ? possibleContacts[Random.Range(0, possibleContacts.Count)] : null;
    }
}
