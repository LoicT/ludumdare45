﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Get Money", menuName = "Random Event/Get Money")]
public class GetMoneyEvent : RandomEvent
{
    public int amount = 1000;

    public override bool CanOccur()
    {
        return true;
    }

    public override void Resolve(int choice)
    {
        Manager.Game.Protagonist.possessions.Money += this.amount;
        Manager.UI.ShowProtagonist();
    }
}