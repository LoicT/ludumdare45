﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Fired", menuName = "Random Event/Work/Fired")]
public class FiredEvent : RandomEvent
{
    public override bool CanOccur()
    {
        return false;
    }

    public override void Resolve(int choice)
    {
        Manager.Professions.QuitProfession();
    }
}
