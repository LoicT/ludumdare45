﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RandomEvent : ScriptableObject
{
    public int probability;

    [MultiLineProperty]
    public string message;
    public List<string> choices;

    public int cooldown;

    public abstract bool CanOccur();
    public abstract void Resolve(int choice);

    public virtual string Message()
    {
        return this.message;
    }
}