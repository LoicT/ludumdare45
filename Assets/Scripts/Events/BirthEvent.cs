﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Birth", menuName = "Random Event/Birth")]
public class BirthEvent : RandomEvent
{
    public static Protagonist PossibleChild;

    public override bool CanOccur()
    {
        return this.Mother() != null;
    }

    public override void Resolve(int choice)
    {
        if (PossibleChild != null)
        {
            switch (choice)
            {
                case 0:
                    Manager.Game.AddToLineage(PossibleChild);
                    break;
                default:
                    break;
            }
        }

        PossibleChild = null;
    }

    public override string Message()
    {
        if (PossibleChild == null)
        {
            PossibleChild = Manager.Game.Protagonist.GiveBirth(this.Mother());
        }

        return this.message
            .Replace("{protagonist}", Manager.Game.Protagonist.name)
            .Replace("{cost}", Mathf.RoundToInt(PossibleChild.possessions.Money).ToString())
            .Replace("{child}", "Child characteristics:\n" + PossibleChild)
            .Replace("{target}", PossibleChild.mother.name);
    }

    private Target Mother()
    {
        return Manager.Game.Protagonist.Acquaintances.Find(x => x.CanGiveBirth());
    }
}
