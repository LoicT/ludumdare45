﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Burnout", menuName = "Random Event/Work/Burnout")]
public class BurnOutEvent : RandomEvent
{
    public int severity;

    public override bool CanOccur()
    {
        return Manager.Game.Protagonist.Job != null &&
            Manager.Game.Protagonist.actionPointsInProfession >= severity;
    }

    public override void Resolve(int choice)
    {
        Manager.Game.Protagonist.actionPointsInProfession = 0;
        Manager.Game.Protagonist.ActionPoints = 0;
    }
}