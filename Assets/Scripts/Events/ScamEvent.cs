﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scam", menuName = "Random Event/Scam")]
public class ScamEvent : RandomEvent
{
    public int cost;
    public float winProbability;
    public RandomEvent acceptEventWin;
    public RandomEvent acceptEventLose;
    public RandomEvent refuseEvent;

    public override bool CanOccur()
    {
        return Manager.Game.Protagonist.possessions.Money >= this.cost;
    }

    public override void Resolve(int choice)
    {
        switch (choice)
        {
            case 0:
                Manager.Game.Protagonist.possessions.Money -= this.cost;
                Manager.UI.ShowProtagonist();

                if (Random.Range(0, 100) < this.winProbability)
                    Manager.RandomEvents.ShowRandomEvent(acceptEventWin);
                else
                    Manager.RandomEvents.ShowRandomEvent(acceptEventLose);
                break;
            default:
                Manager.RandomEvents.ShowRandomEvent(refuseEvent);
                break;
        }
    }
}