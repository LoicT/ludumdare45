using UnityEngine;

public abstract class Action : ScriptableObject
{
    public int apCost = 1;
    public int moneyCost = 0;

    public virtual void PerformAction(object parameter = null)
    {
        Manager.Game.Protagonist.possessions.Money -= this.moneyCost;
        Manager.Game.Protagonist.ActionPoints -= this.apCost;
    }

    public virtual bool IsPossible()
    {
        return Manager.Game.Protagonist.ActionPoints >= this.apCost && Manager.Game.Protagonist.possessions.Money >= this.moneyCost;
    }
}