﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GoToAction : Action
{
    public bool needsVehicle = false;

    public override bool IsPossible()
    {
        return base.IsPossible() && (!needsVehicle || Manager.Game.Protagonist.possessions.HasVehicle());
    }
}
