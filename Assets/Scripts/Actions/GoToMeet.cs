﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Go to", menuName = "Action/Go to (meet)")]
public class GoToMeet : GoToAction
{
    public int newEncountersPercentage;
    public DialogueManager.Setting setting;

    public override void PerformAction(object parameter = null)
    {
        base.PerformAction(parameter);
        bool newEncounter = Manager.Game.Protagonist.Acquaintances.Count == 0 || Random.Range(0, 100) <= this.newEncountersPercentage;
        Target target = newEncounter ? CharacterFactory.GenerateTargetFromPrestige(Random.Range(0f, Characteristics.MAX_PRESTIGE)) : Manager.Game.Protagonist.Acquaintances[Random.Range(0, Manager.Game.Protagonist.Acquaintances.Count)];
        Manager.Dialogue.StartDialogue(target, Manager.Dialogue.GenerateConversation(setting, !newEncounter, false));
    }
}