﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Go to", menuName = "Action/Go to (learn)")]
public class GoToLearn : GoToAction
{
    public Characteristics.Name characteristic;
    public int quantity;

    public override void PerformAction(object parameter = null)
    {
        base.PerformAction(parameter);
        Manager.Game.Protagonist.characteristics.SetCharacteristicValue(this.characteristic, Manager.Game.Protagonist.characteristics.GetCharacteristicValue(this.characteristic) + quantity);
        Manager.Game.Protagonist.UpdateBody();
        Manager.UI.ShowProtagonist();
    }
}
