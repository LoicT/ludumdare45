﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Call", menuName = "Action/Call")]
public class PhoneAction : Action
{
    public override void PerformAction(object parameter)
    {
        if (((Target) parameter) == null)
        {
            throw new System.Exception("Unknown number, cannot call.");
        }

        base.PerformAction(parameter);

        Target target = (Target) parameter;
        target.Call();
        Manager.Dialogue.StartDialogue(target, Manager.Dialogue.GenerateConversation(DialogueManager.Setting.Phone, true, false));
    }
}
