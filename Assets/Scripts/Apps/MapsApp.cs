﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapsApp : App
{
    public List<Action> actions;

    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);
        foreach (Action action in this.actions)
        {
            ActionButton button = Instantiate(Manager.UI.actionPrefab, Manager.UI.appsUIRoot).GetComponent<ActionButton>();
            button.Initialize(action);
        }
    }
}
