using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class AppearanceItem
{
    public string name;
    public Appearance appearance;
    public TotalBody.Name part;
}

public class BeautyParlorApp : App
{
    public List<AppearanceItem> items;

    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);
        foreach (AppearanceItem item in this.items)
        {
            if (!Manager.Game.Protagonist.totalBody.HasAppearance(item.appearance, item.part))
            {
                BodyPartButton button = Instantiate(Manager.UI.beautyParlorPrefab, Manager.UI.appsUIRoot).GetComponent<BodyPartButton>();
                button.Initialize(item);
            }
        }

        Text appInfo = this.AddAppInfo();
        if (appInfo != null)
        {
            appInfo.text = "There's nothing available for you to buy. Come again!";
        }
    }
}