using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class JobApp : App
{
    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);

        if (Manager.Game.Protagonist.Job != null)
        {
            JobInfo jobInfo = Instantiate(Manager.UI.jobInfoPrefab, Manager.UI.appsUIRoot).GetComponent<JobInfo>();
            jobInfo.Initialize(Manager.Game.Protagonist.Job);
        }

        foreach (Profession job in Manager.Professions.AvailableProfessions())
        {
            if (job != Manager.Game.Protagonist.Job && job.CanTake())
            {
                JobButton button = Instantiate(Manager.UI.jobPrefab, Manager.UI.appsUIRoot).GetComponent<JobButton>();
                button.Initialize(job);
            }
        }

        Text appInfo = this.AddAppInfo();
        if (appInfo != null)
        {
            appInfo.text = "There are no jobs available.\n\n<b>Reminder:</b> a job becomes available\nagain 5 months after quitting.";
        }
    }
}