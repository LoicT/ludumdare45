﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StalkedexApp : App
{
    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);
        foreach (Target target in Manager.Game.Protagonist.Acquaintances)
        {
            TargetPanel panel = Instantiate(Manager.UI.targetPrefab, Manager.UI.appsUIRoot).GetComponent<TargetPanel>();
            panel.Initialize(target);
        }

        Text appInfo = this.AddAppInfo();
        if (appInfo != null)
        {
            appInfo.text = "You don't know any girls yet.";
        }
    }
}
