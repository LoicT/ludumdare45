﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class App : MonoBehaviour
{
    public abstract void Open();

    public Text AddAppInfo()
    {
        if (Manager.UI.appsUIRoot.childCount == 0)
        {
            Text text = Instantiate(Manager.UI.appsInfoText, Manager.UI.appsUIRoot).GetComponent<Text>();
            return text;
        }

        return null;
    }
}
