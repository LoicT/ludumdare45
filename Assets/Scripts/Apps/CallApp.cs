﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallApp : App
{
    public Action action;

    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);
        foreach (Target target in Manager.Game.Protagonist.Acquaintances)
        {
            if (target.CanCall())
            {
                ActionButton button = Instantiate(Manager.UI.actionPrefab, Manager.UI.appsUIRoot).GetComponent<ActionButton>();
                button.Initialize(action, target);
            }
        }

        Text appInfo = this.AddAppInfo();
        if (appInfo != null)
        {
            appInfo.text = "You have nobody to call.\n\n<b>Reminder:</b> you can call each girl\nonce every three months.";
        }
    }
}
