using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class StoreApp : App
{
    public override void Open()
    {
        Manager.UI.appsUIObject.SetActive(true);
        foreach (Item item in Manager.Item.GetBuyableItems())
        {
            ItemButton button = Instantiate(Manager.UI.itemPrefab, Manager.UI.appsUIRoot).GetComponent<ItemButton>();
            button.Initialize(item);
        }

        Text appInfo = this.AddAppInfo();
        if (appInfo != null)
        {
            appInfo.text = "There's nothing available for you to buy. Come again!";
        }
    }
}